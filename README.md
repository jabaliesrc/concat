# Concat

## Install

```
pip install git+ssh://git@gitlab.com/jabaliesrc/concat.git@main
```

## Use

```
concat --path /mnt/jrc/crudo/JRC-SANMA-11-09 --extension MP4 --output jrc-bandurrias.mp4
```

will scan for `*.MP4` on `/mnt/jrc/crudo/JRC-SANMA-11-09` and run ffmpeg concat demuxer to output `/mnt/jrc/crudo/jrc-bandurrias.mp4`.
