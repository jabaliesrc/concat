import ffmpeg
import click
import os


@click.command()
@click.option('--path', required=True, help='Path to folder with videos.')
@click.option('--extension', default='mp4', help='Input video extension')
@click.option('--output', default='video.mp4', help='Output video')
def concat(path, extension, output):
    input_vids = []
    for (dirpath, dirnames, filenames) in os.walk(path):
        input_vids = input_vids + [
            os.path.join(path, f) 
            for f in filenames if f.endswith(extension)
        ]
    input_vids.sort()
    videos_txt = os.path.join(path, 'videos.txt')
    if os.path.exists(videos_txt):
        os.remove(videos_txt)
    with open(videos_txt, 'w') as videos_file:
        for video_file in input_vids:
            videos_file.write(f'file \'{video_file}\'\n')
    out, err = (
         ffmpeg.input(videos_txt, format='concat', safe=0)
               .output(output, c='copy')
               .run()
    )

if __name__ == '__main__':
    concat()